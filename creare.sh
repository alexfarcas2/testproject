#!/bin/bash

mkdir f1
cd f1
mkdir f2
mkdir f3
cd f2
mkdir f4
cd f4
touch 1.txt 2.txt 3.txt
find . -name "*.txt" ! -name "1.txt" -type f -delete
cd ..
mkdir f5
cd f5
touch 1.txt 2.txt 3.txt
find . -name "*.txt" ! -name "1.txt" -type f -delete
cd ..
cd ..
cd f3
mkdir f5
cd f5
touch 1.txt 2.txt 3.txt
find . -name "*.txt" ! -name "1.txt" -type f -delete
cd ..
mkdir f6
cd f6
touch 1.txt 2.txt 3.txt
find . -name "*.txt" ! -name "1.txt" -type f -delete